import main.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyServiceTest {

    private FamilyService module;

    List<Family> families = new ArrayList<>(List.of(
            new Family(new Human("Елена", "Пупенко", "17/04/1974"), new Human("Василий", "Пупенко", "09/08/1973")),
            new Family(new Human("Анастасия", "Тестюк", "18/04/1963"), new Human("Игорь", "Тестюк", "20/04/1964")),
            new Family(new Human("Мария", "Тестовская", "22/06/1995"), new Human("Петр", "Тестовский", "13/12/1988"))
    ));

    FamilyServiceTest() throws ParseException {
    }

    @BeforeEach
    void setUp() {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        collectionFamilyDao.setFamilyList(families);
        module = new FamilyService(collectionFamilyDao);
    }

    @Test
    void getAllFamilies() {
        List<Family> actual = module.getAllFamilies();
        List<Family> expected = families;
        assertEquals(expected, actual);
    }

    @Test
    void displayAllFamilies() {
        List<Family> actual = module.displayAllFamilies();
        List<Family> expected = families;
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesBiggerThen() {
        List<Family> actual = module.getFamiliesBiggerThen(1);
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamilyInt() > 1) {
                newFamilies.add(family);
            }
        });
        List<Family> expected = newFamilies;
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesLessThen() {
        List<Family> actual = module.getFamiliesLessThen(3);
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamilyInt() < 3) {
                newFamilies.add(family);
            }
        });
        List<Family> expected = newFamilies;
        assertEquals(expected, actual);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int actual = module.countFamiliesWithMemberNumber(2);
        int expected = 3;
        assertEquals(expected, actual);
    }

    @Test
    void createNewFamily() {
        int size = families.size();
        module.createNewFamily(new Human(), new Human());
        int actual = families.size();
        int expected = size + 1;
        assertEquals(expected, actual);
    }

    @Test
    void deleteFamilyByIndex() {
        Family actual = families.get(1);
        module.deleteFamilyByIndex(0);
        Family expected = families.get(0);
        assertEquals(expected, actual);
    }

    @Test
    void bornChild() throws ParseException {
        module.bornChild(families.get(0), "Наталия", "Юрий");
        String childName = families.get(0).getChildren().get(0).getName();
        boolean actual = childName.equals("Наталия") || childName.equals("Юрий");
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void adoptChild() throws ParseException {
        Human actual = module.adoptChild(families.get(1), new Human("Валерий", "Найденко", "14/10/2022")).getChildren().get(0);
        Human expected = families.get(1).getChildren().get(0);
        assertEquals(expected, actual);
    }

    @Test
    void deleteAllChildrenOlderThen() throws ParseException {
        families.get(2).getChildren().add(new Human("name1", "surname1", "18/03/2021"));
        families.get(2).getChildren().add(new Human("name2", "surname2", "25/07/2020"));
        boolean actual;
        int age = 2;
        module.deleteAllChildrenOlderThen(age);
        List<Human> ChildrenOlderThen = new ArrayList<>();
        int currentYear = LocalDate.now().getYear();
//        int birthYear =
        for (int i = 0; i < families.get(2).getChildren().size(); i++) {
            if (currentYear - families.get(2).getChildren().get(i).getBirthDate() <= age) {
                ChildrenOlderThen.add(families.get(2).getChildren().get(i));
            }
        }
        actual = ChildrenOlderThen.size() == 1;
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void count() {
        int actual = module.count();
        int expected = families.size();
        assertEquals(expected, actual);
    }

    @Test
    void getFamilyById() {
        Family actual = module.getFamilyById(2);
        Family expected = families.get(2);
        assertEquals(expected, actual);
    }

    @Test
    void getPets() {
        Set<Pet> actual = module.getPets(0);
        Set<Pet> expected = families.get(0).getPet();
        assertEquals(expected, actual);
    }

    @Test
    void addPet() {
        Pet pet = new Dog("Бобик", 4, 40);
        module.addPet(0, pet);
        Set<Pet> actual = new HashSet<>(Set.of(pet));
        Set<Pet> expected = families.get(0).getPet();
        assertEquals(expected, actual);
    }
}