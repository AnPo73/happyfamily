import main.Dog;
import main.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static main.Species.DOG;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class PetTest {

    private Pet module;

    @BeforeEach
    void setUp() {
        module = new Pet() {
            @Override
            public void respond() {
            }
        };
    }

    @Test
    void testToString() {
        String actual = module.toString();
        String expected = "{species=" + module.getSpecies() +
                ", nickname='" + module.getNickname() + '\'' +
                ", age=" + module.getAge() +
                ", trickLevel=" + module.getTrickLevel() +
                ", habits=" + module.getHabits() +
                '}';
        assertEquals(expected, actual);
    }

    @Test
    void testToStringNegative() {
        String actual = module.toString();
        String expected = module.getSpecies() + "{" +
                ", age=" + module.getAge() +
                ", trickLevel=" + module.getTrickLevel() +
                ", habits=" + module.getHabits() +
                '}';
        assertNotEquals(expected, actual);
    }

    @Test
    void testInstance() {
        Boolean actual = (new Dog() instanceof Pet);
        Boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void testSpecies() {
        Dog dog = new Dog();
        dog.setSpecies(DOG);
        String actual = String.valueOf(dog.getSpecies());
        String expected = "DOG";
        assertEquals(expected, actual);
    }

    @Test
    void testSpeciesNegative() {
        String actual = String.valueOf(new Dog().getSpecies());
        String expected = "UNKNOWN";
        assertEquals(expected, actual);
    }
}
