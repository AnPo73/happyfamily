import main.Human;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class HumanTest {

    private Human module;

    @BeforeEach
    void setUp() {
        module = new Human();
    }

    @Test
    void testToString() throws ParseException {
        module = new Human("name", "surname", "11/11/2011");
        String actual = module.toString();
        String expected = "Human{" +
                "name='" + module.getName() + '\'' +
                ", surname='" + module.getSurname() + '\'' +
                ", birthDate=" + module.formattedBirthDate() +
                ", iq=" + module.getIq() +
                ", schedule=" + module.getSchedule() +
                '}';
        assertEquals(expected, actual);
    }

    @Test
    void testToStringNegative() {
        String actual = module.toString();
        String expected = "Human{" +
                "name='" + module.getName() + '\'' +
                ", surname='" + module.getSurname() + '\'' +
                ", iq=" + module.getIq() +
                ", schedule=" + module.getSchedule() +
                '}';
        assertNotEquals(expected, actual);
    }

    @Test
    void describeAge() {
        String actual = module.describeAge();
        LocalDate birthDate = LocalDate.ofInstant(Instant.ofEpochMilli(module.getBirthDate()), ZoneId.systemDefault());
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(birthDate, currentDate);
        String expected = period.getYears() + " лет, " + period.getMonths() + " месяцев, " + period.getDays() + " дней";
        assertEquals(expected, actual);
    }
}
