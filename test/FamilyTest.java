import main.DomesticCat;
import main.Family;
import main.Human;
import main.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.*;

import static main.DayOfWeek.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class FamilyTest {

    private Family module;
    private Human mother1;
    private Human father1;
    private Family family1;
    private Human daughter1;
    private Human son1;
    private DomesticCat cat1;

    @BeforeEach
    public void setUp() throws ParseException {

        module = new Family();

        HashMap<String, String> scheduleMother = new HashMap<>();
        scheduleMother.put(MONDAY.name(), "Мама 1");
        scheduleMother.put(TUESDAY.name(), "Мама 2");
        scheduleMother.put(WEDNESDAY.name(), "Мама 3");
        scheduleMother.put(THURSDAY.name(), "Мама 4");
        scheduleMother.put(FRIDAY.name(), "Мама 5");
        scheduleMother.put(SATURDAY.name(), "Мама 6");
        scheduleMother.put(SUNDAY.name(), "Мама 7");

        HashMap<String, String> scheduleFather = new HashMap<>();
        scheduleFather.put(MONDAY.name(), "Папа 1");
        scheduleFather.put(TUESDAY.name(), "Папа 2");
        scheduleFather.put(WEDNESDAY.name(), "Папа 3");
        scheduleFather.put(THURSDAY.name(), "Папа 4");
        scheduleFather.put(FRIDAY.name(), "Папа 5");
        scheduleFather.put(SATURDAY.name(), "Папа 6");
        scheduleFather.put(SUNDAY.name(), "Папа 7");

        HashMap<String, String> scheduleDaughter = new HashMap<>();
        scheduleDaughter.put(MONDAY.name(), "Доця 1");
        scheduleDaughter.put(TUESDAY.name(), "Доця 2");
        scheduleDaughter.put(WEDNESDAY.name(), "Доця 3");
        scheduleDaughter.put(THURSDAY.name(), "Доця 4");
        scheduleDaughter.put(FRIDAY.name(), "Доця 5");
        scheduleDaughter.put(SATURDAY.name(), "Доця 6");
        scheduleDaughter.put(SUNDAY.name(), "Доця 7");

        HashMap<String, String> scheduleSon = new HashMap<>();

        Set<String> habitsCat1 = new HashSet<>();
        habitsCat1.add("есть");
        habitsCat1.add("пить");
        habitsCat1.add("играть");
        habitsCat1.add("спать");

        Set<String> habitsDog1 = new HashSet<>();
        habitsDog1.add("есть");
        habitsDog1.add("пить");
        habitsDog1.add("гулять");
        habitsDog1.add("спать");

        mother1 = new Human("Оксана", "Медынская", "17/04/1974", 100, scheduleMother);

        father1 = new Human();
        father1.setName("Андрей");
        father1.setSurname("Погановский");
        father1.setIq(100);
        father1.setBirthDate("09/08/1973");
        father1.setSchedule(scheduleFather);

        family1 = new Family(mother1, father1);
        mother1.setFamily(family1);
        father1.setFamily(family1);

        cat1 = new DomesticCat("Лапка", 6, 60, habitsCat1);
        Set<Pet> pet1 = new HashSet<>(Set.of(cat1));
        family1.setPet(pet1);

        daughter1 = new Human("Наталия", "Погановская", "02/12/2009", 100, scheduleDaughter);
        son1 = new Human("Петр", "Погановский", "06/01/2022", 100, scheduleSon);

        List<Human> children1 = new ArrayList<Human>(Set.of(daughter1, son1));
        daughter1.setFamily(family1);
        son1.setFamily(family1);
        family1.setChildren(children1);

    }

    @Test
    void testToString() {
        String actual = module.toString();
        String expected = "Family" + "\n" +
                "mother=" + module.getMother() + "\n" +
                "father=" + module.getFather() + "\n" +
                "children=" + module.getChildren() + "\n" +
                "pet=" + module.getPet();
        assertEquals(expected, actual);
    }

    @Test
    void testToStringNegative() {
        String actual = module.toString();
        String expected = "Family{" +
                "mother=" + module.getMother() +
                ", father=" + module.getFather() +
                ", children=" + module.getChildren() +
                '}';
        assertNotEquals(expected, actual);
    }

    @Test
    void testDeleteChild() {
        family1.deleteChild(son1);
        int actual = family1.getChildren().size();
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteChildNegative() {
        family1.deleteChild(son1);
        family1.deleteChild(son1);
        int actual = family1.getChildren().size();
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteChildByIndex() {
        family1.deleteChild(1);
        int actual = family1.getChildren().size();
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteChildByIndexNegative() {
        family1.deleteChild(1);
        family1.deleteChild(100);
        int actual = family1.getChildren().size();
        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    void testAddChild() {
        family1.addChild(new Human());
        int actual = family1.getChildren().size();
        int expected = 3;
        assertEquals(expected, actual);

    }

    @Test
    void testAddChildNegative() {
        family1.addChild(son1);
        int actual = family1.getChildren().size();
        int expected = 2;
        assertEquals(expected, actual);
    }

    @Test
    void testCountFamily() {
        family1.deleteChild(son1);
        String actual = family1.countFamily();
        String expected = "В этой семье 3 человека";
        assertEquals(expected, actual);
    }

    @Test
    void testCountFamilyNegative() {
        family1.deleteChild(son1);
        family1.deleteChild(son1);
        family1.deleteChild(daughter1);
        String actual = family1.countFamily();
        String expected = "В этой семье 2 человека";
        assertEquals(expected, actual);
    }
}
