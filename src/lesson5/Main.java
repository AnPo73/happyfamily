package lesson5;

import main.*;

import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws ParseException {

//        for (int i = 0; i < 130000; i++) {
//            Human human = new Human();
//        }

        List<Family> families = new LinkedList<>();
        families.add(new Family(new Human("Елена", "Пупенко", "17/04/1974"), new Human("Василий", "Пупенко", "09/08/1973")));
        families.add(new Family(new Human("Анастасия", "Тестюк", "18/04/1963"), new Human("Игорь", "Тестюк", "20/04/1964")));
        families.add(new Family(new Human("Мария", "Тестовская", "22/06/1995"), new Human("Петр", "Тестовский", "13/12/1988")));

        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        collectionFamilyDao.setFamilyList(families);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        System.out.println();
        System.out.println("-=getAllFamilies=-");
        familyController.getAllFamilies();
        families.forEach(System.out::println);
        System.out.println();
        System.out.println("-=displayAllFamilies=-");
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("-=getFamiliesBiggerThen=-");
        familyController.getFamiliesBiggerThen(1);
        System.out.println();
        System.out.println("-=getFamiliesLessThen=-");
        familyController.getFamiliesLessThen(3);
        System.out.println();
        System.out.println("-=countFamiliesWithMemberNumber=-");
        System.out.println(familyController.countFamiliesWithMemberNumber(2));
        System.out.println();
        System.out.println("-=createNewFamily=-");
        System.out.println("BEFORE:");
        System.out.println(families.get(families.size() - 1));
        System.out.println("AFTER:");
        familyController.createNewFamily(new Human("Татьяна", "Тестович", "08/09/1986"), new Human("Сергей", "Тестович", "01/02/1984"));
        System.out.println(families.get(families.size() - 1));
        System.out.println();
        System.out.println("-=deleteFamilyByIndex=-");
        System.out.println("BEFORE:");
        System.out.println(families.get(0));
        familyController.deleteFamilyByIndex(0);
        System.out.println("AFTER:");
        System.out.println(families.get(0));
        System.out.println();
        System.out.println("-=bornChild=-");
        System.out.println("BEFORE:");
        System.out.println(families.get(0));
        familyController.bornChild(families.get(0), "Наталия", "Юрий");
        System.out.println("AFTER:");
        System.out.println(families.get(0));
        System.out.println();
        System.out.println("-=adoptChild=-");
        System.out.println("BEFORE:");
        System.out.println(families.get(1));
        System.out.println("AFTER:");
        familyController.adoptChild(families.get(1), new Human("Валерий", "Найденко", "14/10/2022", Gender.MALE));
        System.out.println(families.get(1));
        System.out.println();
        System.out.println("-=DeleteAllChildrenOlderThen=-");
        families.get(2).getChildren().add(new Human("name1", "surname1", "18/03/2021", Gender.FEMALE));
        families.get(2).getChildren().add(new Human("name2", "surname2", "25/07/2020", Gender.MALE));
        System.out.println("BEFORE:");
        familyController.displayAllFamilies();
        familyController.deleteAllChildrenOlderThen(2);
        System.out.println("AFTER:");
        familyController.displayAllFamilies();
        System.out.println();
        System.out.println("-=count=-");
        System.out.println(familyController.count());
        System.out.println();
        System.out.println("-=getFamilyById=-");
        System.out.println(familyController.getFamilyById(2));
        System.out.println();
        System.out.println("-=getPets=-");
        familyController.addPet(0, new DomesticCat("Лапка", 6, 60));
        System.out.println(familyController.getPets(0));
        System.out.println();
        System.out.println("-=addPet=-");
        familyController.addPet(0, new Dog("Бобик", 4, 40));
        System.out.println(familyController.getFamilyById(0));
    }
}