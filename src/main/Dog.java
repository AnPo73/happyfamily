package main;

import java.io.Serializable;
import java.util.Set;

public class Dog extends Pet implements Foul, Serializable {

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel) {
        super(nickname, age, trickLevel);
        species = Species.DOG;
    }

    public Dog() {
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + Species.DOG + " " + this.getNickname() + ". Я соскучился!");
    }

    @Override
    public void foul() {
        System.out.println("Ой... пора делать ноги...");
    }
}