package main;

import java.text.ParseException;

public final class AdoptedChild extends Human {
    public AdoptedChild(String name, String surname, String birthDate, int iq) throws ParseException {
        super(name, surname, birthDate, iq);
    }
}
