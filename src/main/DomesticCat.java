package main;

import java.io.Serializable;
import java.util.Set;

public class DomesticCat extends Pet implements Foul, Serializable {

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.DOMESTIC_CAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel) {
        super(nickname, age, trickLevel);
        species = Species.DOMESTIC_CAT;
    }

    public DomesticCat() {
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + Species.DOMESTIC_CAT + " " + this.getNickname() + ". Поиграем?");
    }

    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}