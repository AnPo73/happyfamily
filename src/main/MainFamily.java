package main;

import java.io.IOException;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class MainFamily {

    public static Scanner console = new Scanner(System.in);

    public static void main(String[] args) throws ParseException, IOException {
        List<Family> families = new LinkedList<>();

        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        collectionFamilyDao.setFamilyList(families);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        String menuItem = "";

        while (!Objects.equals(menuItem, "12")) {
            System.out.println();
            System.out.println("1. Загрузить тестовые данные");
            System.out.println("2. Загрузить список всех семей");
            System.out.println("3. Отобразить список всех семей");
            System.out.println("4. Отобразить список семей, где количество людей больше заданного");
            System.out.println("5. Отобразить список семей, где количество людей меньше заданного");
            System.out.println("6. Подсчитать количество семей, где количество людей равно заданному");
            System.out.println("7. Создать новую семью");
            System.out.println("8. Удалить семью по индексу в общем списке");
            System.out.println("9. Редактировать семью по индексу в общем списке");
            System.out.println("10. Удалить всех детей старше заданного возраста");
            System.out.println("11. Сохранить список всех семей");
            System.out.println("12. Выход");
            System.out.println();
            System.out.print("Сделайте Ваш выбор (пункт меню от 1 до 12): ");
            menuItem = console.nextLine();

            switch (menuItem) {

                case "1":
                    if (!(familyController.getAllFamilies().size() == 0)) {
                        System.err.println("Список семей не пустой, нет необходимости в загрузке тестовых данных");
                    } else {
                        familyController.loadTestData(families);
                    }
                    break;

                case "2":
                    familyController.loadData(families);
                    break;

                case "3":
                    try {
                        if (familyController.getAllFamilies().size() == 0) {
                            throw new NullPointerException("Список семей пустой, необходимо заполнить данными");
                        } else {
                            familyController.displayAllFamilies();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "4":
                    try {
                        if (familyController.getAllFamilies().size() == 0) {
                            throw new NullPointerException("Список семей пустой, необходимо заполнить данными");
                        } else {
                            System.out.print("Введите количество людей (целое число): ");
                            if (!console.hasNextInt()) {
                                System.err.println("Невалидное значение, попробуйте еще раз");
                                console.nextLine();
                            } else {
                                int size3 = console.nextInt();
                                console.nextLine();
                                if (familyController.getFamiliesBiggerThen(size3).size() == 0) {
                                    System.err.println("Количество семей больше " + size3 + " людей равно 0");
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "5":
                    try {
                        if (familyController.getAllFamilies().size() == 0) {
                            throw new NullPointerException("Список семей пустой, необходимо заполнить данными");
                        } else {
                            System.out.print("Введите количество людей (целое число): ");
                            if (!console.hasNextInt()) {
                                System.err.println("Невалидное значение, попробуйте еще раз");
                                console.nextLine();
                            } else {
                                int size4 = console.nextInt();
                                console.nextLine();
                                if (familyController.getFamiliesLessThen(size4).size() == 0) {
                                    System.err.println("Количество семей меньше " + size4 + " людей равно 0");
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "6":
                    try {
                        if (familyController.getAllFamilies().size() == 0) {
                            throw new NullPointerException("Список семей пустой, необходимо заполнить данными");
                        } else {
                            System.out.print("Введите количество людей (целое число): ");
                            if (!console.hasNextInt()) {
                                System.err.println("Невалидное значение, попробуйте еще раз");
                                console.nextLine();
                            } else {
                                int size5 = console.nextInt();
                                console.nextLine();
                                System.out.println(familyController.countFamiliesWithMemberNumber(size5));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "7":
                    try {
                        System.out.print("Введите имя матери: ");
                        String motherName = console.nextLine();
                        System.out.print("Введите фамилию матери: ");
                        String motherSurname = console.nextLine();
                        System.out.print("Введите год рождения матери (ГГГГ): ");
                        String motherBirthYear = console.nextLine();
                        System.out.print("Введите месяц рождения матери (ММ): ");
                        String motherBirthMonth = console.nextLine();
                        System.out.print("Введите день рождения матери (ДД): ");
                        String motherBirthDay = console.nextLine();
                        String motherBirthDate = motherBirthDay + "/" + motherBirthMonth + "/" + motherBirthYear;
                        System.out.print("Введите IQ матери (целое число от 0 до 100): ");
                        int motherIQ = console.nextInt();
                        console.nextLine();

                        System.out.print("Введите имя отца: ");
                        String fatherName = console.nextLine();
                        System.out.print("Введите фамилию отца: ");
                        String fatherSurname = console.nextLine();
                        System.out.print("Введите год рождения отца (ГГГГ): ");
                        String fatherBirthYear = console.nextLine();
                        System.out.print("Введите месяц рождения отца (ММ): ");
                        String fatherBirthMonth = console.nextLine();
                        System.out.print("Введите день рождения отца (ДД): ");
                        String fatherBirthDay = console.nextLine();
                        String fatherBirthDate = fatherBirthDay + "/" + fatherBirthMonth + "/" + fatherBirthYear;
                        System.out.print("Введите IQ отца (целое число от 0 до 100): ");
                        int fatherIQ = console.nextInt();
                        console.nextLine();

                        familyController.createNewFamily(new Human(motherName, motherSurname, motherBirthDate, motherIQ), new Human(fatherName, fatherSurname, fatherBirthDate, fatherIQ));
                        System.out.println(families.get(families.size() - 1));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "8":
                    try {
                        if (familyController.getAllFamilies().size() == 0) {
                            throw new NullPointerException("Список семей пустой, необходимо заполнить данными");
                        } else {
                            System.out.print("Введите порядковый номер семьи (целое число): ");
                            if (!console.hasNextInt()) {
                                System.err.println("Невалидное значение, попробуйте еще раз");
                                console.nextLine();
                            } else {
                                int familyIndex7 = console.nextInt();
                                console.nextLine();
                                familyController.deleteFamilyByIndex(familyIndex7);
                                familyController.displayAllFamilies();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "9":
                    while (!Objects.equals(menuItem, "3")) {
                        System.out.println();
                        System.out.println("1. Родить ребенка");
                        System.out.println("2. Усыновить ребенка");
                        System.out.println("3. Вернуться в главное меню");
                        System.out.println();
                        System.out.print("Сделайте Ваш выбор (пункт меню от 1 до 3): ");
                        menuItem = console.nextLine();
                        switch (menuItem) {

                            case "1":
                                try {
                                    System.out.print("Введите порядковый номер семьи (целое число): ");
                                    if (!console.hasNextInt()) {
                                        System.err.println("Невалидное значение, попробуйте еще раз");
                                        console.nextLine();
                                    } else {
                                        int familyIndex81 = console.nextInt();
                                        console.nextLine();
                                        Family family81 = familyController.getFamilyById(familyIndex81);
                                        if (family81.countFamilyInt() > FamilyOverflowException.familySize) {
                                            throw new FamilyOverflowException("В семье больше " + FamilyOverflowException.familySize + " людей");
                                        } else {
                                            System.out.print("Введите имя девочки: ");
                                            String girlName = console.nextLine();
                                            System.out.print("Введите имя мальчика: ");
                                            String boyName = console.nextLine();
                                            familyController.bornChild(family81, girlName, boyName);
                                            familyController.getFamilyById(familyIndex81).prettyFormat();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case "2":
                                try {
                                    System.out.print("Введите порядковый номер семьи (целое число): ");
                                    if (!console.hasNextInt()) {
                                        System.err.println("Невалидное значение, попробуйте еще раз");
                                        console.nextLine();
                                    } else {
                                        int familyIndex82 = console.nextInt();
                                        console.nextLine();
                                        Family family82 = familyController.getFamilyById(familyIndex82);
                                        if (family82.countFamilyInt() > FamilyOverflowException.familySize) {
                                            throw new FamilyOverflowException("В семье больше " + FamilyOverflowException.familySize + " людей");
                                        } else {
                                            System.out.print("Введите имя ребенка: ");
                                            String childName = console.nextLine();
                                            System.out.print("Введите фамилию ребенка: ");
                                            String childSurname = console.nextLine();
                                            System.out.print("Введите дату рождения ребенка (ДД/ММ/ГГГГ): ");
                                            String childBirthDate = console.nextLine();
                                            System.out.print("Введите IQ ребенка (целое число от 0 до 100): ");
                                            int childIQ = console.nextInt();
                                            console.nextLine();
                                            System.out.print("Введите пол ребенка (Ж/М): ");
                                            String childGender = console.nextLine();
                                            childGender = childGender.toUpperCase().trim();
                                            familyController.adoptChild(family82, new Human(childName, childSurname, childBirthDate, childIQ,
                                                    (childGender.equals("Ж") ? Gender.FEMALE : Gender.MALE))).prettyFormat();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case "3":
                                break;
                        }
                    }
                    break;

                case "10":
                    try {
                        if (familyController.getAllFamilies().size() == 0) {
                            throw new NullPointerException("Список семей пустой, необходимо заполнить данными");
                        } else {
                            System.out.print("Введите возраст (целое число): ");
                            if (!console.hasNextInt()) {
                                System.err.println("Невалидное значение, попробуйте еще раз");
                                console.nextLine();
                            } else {
                                int childAge = console.nextInt();
                                console.nextLine();
                                familyController.deleteAllChildrenOlderThen(childAge);
                                familyController.displayAllFamilies();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case "11":
                    familyController.saveData(families);
                    System.err.println("Список всех семей сохранен");
                    break;

                case "12":
                    System.exit(0);

                default:
                    System.err.println("Невалидное значение, попробуйте еще раз");
            }
        }
    }
}