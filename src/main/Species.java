package main;

public enum Species {
    DOG,
    DOMESTIC_CAT,
    FISH,
    ROBO_CAT,
    UNKNOWN
}