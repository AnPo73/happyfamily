package main;

import java.io.Serializable;
import java.util.Set;

public class RoboCat extends Pet implements Serializable {

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.ROBO_CAT;
    }

    public RoboCat() {
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + Species.ROBO_CAT + " " + this.getNickname() + ". Искусственный интеллект однако.");
    }
}