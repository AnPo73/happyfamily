package main;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThen(int size) {
        return familyService.getFamiliesBiggerThen(size);
    }

    public List<Family> getFamiliesLessThen(int size) {
        return familyService.getFamiliesLessThen(size);
    }

    public int countFamiliesWithMemberNumber(int size) {
        return familyService.countFamiliesWithMemberNumber(size);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String girlName, String boyName) throws ParseException {
        return familyService.bornChild(family, girlName, boyName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Family> loadData(List<Family> families) {
        return familyService.loadData(families);
    }

    public List<Family> loadTestData(List<Family> families) {
        return familyService.loadTestData(families);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void saveData(List<Family> families) {
        familyService.saveData(families);
    }
}