package main;

public final class Man extends Human {

    @Override
    public void greetPet(String petName) {
        System.out.println("Привет, " + petName + ". Как твои дела?");
    }

    public void repairCar() {
        System.out.println("Надо починить авто");
    }
}