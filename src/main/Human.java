package main;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human implements Serializable {

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<String, String> schedule;
    private Family family;
    private Gender gender;

    public Human() {
    }

    public Human(String name, String surname, String birthDate) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = parsedBirthDate(birthDate);
    }

    public Human(String name, String surname, String birthDate, Gender gender) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = parsedBirthDate(birthDate);
        this.gender = gender;
    }

    public Human(String name, String surname, String birthDate, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = parsedBirthDate(birthDate);
        this.iq = iq;
    }

    public Human(String name, String surname, String birthDate, int iq, Gender gender) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = parsedBirthDate(birthDate);
        this.iq = iq;
        this.gender = gender;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = parsedBirthDate(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule, Gender gender) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = parsedBirthDate(birthDate);
        this.iq = iq;
        this.schedule = schedule;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) throws ParseException {
        this.birthDate = parsedBirthDate(birthDate);
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + formattedBirthDate() +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, family);
    }

    @Override
    protected void finalize() {
        System.out.println("Удаляется объект класса Human");
    }

    public int getYear() {
        String dateFormat = new SimpleDateFormat("yyyy").format(birthDate);
        return Integer.parseInt(dateFormat);
    }

    public String prettyFormat() {
        return "{name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate'=" + formattedBirthDate() + '\'' +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    public void greetPet(String petName) {
        System.out.println("Привет, " + petName + ".");
    }

    public void describePet(Pet pet) {
        System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + (pet.getTrickLevel() > 50 ? "очень хитрый." : "почти не хитрый."));
    }

    public String describeAge() {
        LocalDate birthDate = LocalDate.ofInstant(Instant.ofEpochMilli(this.birthDate), ZoneId.systemDefault());
        LocalDate currentDate = LocalDate.now();
        Period period = Period.between(birthDate, currentDate);
        return (period.getYears() + " лет, " + period.getMonths() + " месяцев, " + period.getDays() + " дней");
    }

    public String formattedBirthDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(birthDate);
    }

    private long parsedBirthDate(String birthDate) throws ParseException {
        Date dateFormat = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate);
        return dateFormat.getTime();
    }
}