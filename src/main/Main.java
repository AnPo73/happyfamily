package main;

import java.text.ParseException;
import java.util.*;

import static main.DayOfWeek.*;

public class Main {
    public static void main(String[] args) throws ParseException {

        List<Family> families = new LinkedList<>();
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        collectionFamilyDao.setFamilyList(families);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        HashMap<String, String> scheduleMother = new LinkedHashMap<>();
        scheduleMother.put(MONDAY.name(), "Мама 1");
        scheduleMother.put(TUESDAY.name(), "Мама 2");
        scheduleMother.put(WEDNESDAY.name(), "Мама 3");
        scheduleMother.put(THURSDAY.name(), "Мама 4");
        scheduleMother.put(FRIDAY.name(), "Мама 5");
        scheduleMother.put(SATURDAY.name(), "Мама 6");
        scheduleMother.put(SUNDAY.name(), "Мама 7");

        HashMap<String, String> scheduleFather = new LinkedHashMap<>();
        scheduleFather.put(MONDAY.name(), "Папа 1");
        scheduleFather.put(TUESDAY.name(), "Папа 2");
        scheduleFather.put(WEDNESDAY.name(), "Папа 3");
        scheduleFather.put(THURSDAY.name(), "Папа 4");
        scheduleFather.put(FRIDAY.name(), "Папа 5");
        scheduleFather.put(SATURDAY.name(), "Папа 6");
        scheduleFather.put(SUNDAY.name(), "Папа 7");

        HashMap<String, String> scheduleDaughter = new LinkedHashMap<>();
        scheduleDaughter.put(MONDAY.name(), "Доця 1");
        scheduleDaughter.put(TUESDAY.name(), "Доця 2");
        scheduleDaughter.put(WEDNESDAY.name(), "Доця 3");
        scheduleDaughter.put(THURSDAY.name(), "Доця 4");
        scheduleDaughter.put(FRIDAY.name(), "Доця 5");
        scheduleDaughter.put(SATURDAY.name(), "Доця 6");
        scheduleDaughter.put(SUNDAY.name(), "Доця 7");

        HashMap<String, String> scheduleSon = new LinkedHashMap<>();

        Set<String> habitsCat1 = new LinkedHashSet<>();
        habitsCat1.add("есть");
        habitsCat1.add("пить");
        habitsCat1.add("играть");
        habitsCat1.add("спать");
        Set<String> habitsDog1 = new LinkedHashSet<>();
        habitsDog1.add("есть");
        habitsDog1.add("пить");
        habitsDog1.add("гулять");
        habitsDog1.add("спать");

        Woman woman1 = new Woman();
        Man man1 = new Man();
        Human mother1 = new Human("Оксана", "Медынская", "17/04/1974", 100, scheduleMother);
        Human father1 = new Human();
        father1.setName("Андрей");
        father1.setSurname("Погановский");
        father1.setIq(100);
        father1.setBirthDate("09/08/1973");
        father1.setSchedule(scheduleFather);

        Family family1 = new Family(mother1, father1);
        mother1.setFamily(family1);
        father1.setFamily(family1);

        DomesticCat cat1 = new DomesticCat("Лапка", 6, 60, habitsCat1);
        Dog dog1 = new Dog("Бобик", 4, 40, habitsDog1);
        Fish fish1 = new Fish();
        Set<Pet> pet1 = new HashSet<>(Set.of(cat1, dog1));
        family1.setPet(pet1);

        Human daughter1 = new Human("Наталия", "Погановская", "02/12/2009", 100, scheduleDaughter);
        Human son1 = new Human("Петр", "Погановский", "06/01/2023", 100, scheduleSon);
        List<Human> children1 = new ArrayList<>(List.of(daughter1));
        daughter1.setFamily(family1);
        family1.setChildren(children1);

        System.out.println();
        System.out.println(mother1);
        System.out.println(father1);
        System.out.println(daughter1);
        System.out.println(cat1);
        System.out.println(dog1);
        System.out.println();

        daughter1.greetPet(cat1.getNickname());
        daughter1.greetPet(dog1.getNickname());
        daughter1.describePet(cat1);
        daughter1.describePet(dog1);
        System.out.println();

        woman1.greetPet(cat1.getNickname());
        man1.greetPet(cat1.getNickname());
        System.out.println();

        cat1.eat();
        fish1.eat();
        cat1.respond();
        dog1.respond();
        fish1.respond();
        cat1.foul();
        dog1.foul();
        System.out.println();

        System.out.println(family1.countFamily());
        System.out.println("***********************");
        family1.addChild(son1);
        System.out.println(family1.countFamily());
        family1.addChild(son1);
        System.out.println(family1.countFamily());
        System.out.println("***********************");
        family1.deleteChild(son1);
        System.out.println(family1.countFamily());
        family1.deleteChild(son1);
        System.out.println(family1.countFamily());
        System.out.println("***********************");
        family1.deleteChild(daughter1);
        System.out.println(family1.countFamily());
        family1.deleteChild(daughter1);
        System.out.println(family1.countFamily());
        System.out.println("***********************");
        family1.addChild(daughter1);
        System.out.println(family1.countFamily());
        System.out.println();

        Human testHuman = new Human("testName", "testSurname", "02/12/2009", 90);
        System.out.println(testHuman);
        System.out.println(testHuman.describeAge());
        AdoptedChild testAdoptedChild = new AdoptedChild("testName", "testSurname", "09/08/1973", 80);
        System.out.println(testAdoptedChild);
        System.out.println(testAdoptedChild.describeAge());
        AdoptedChild testAdoptedChild1 = new AdoptedChild("testName1", "testSurname1", "1983", 70);
        System.out.println(testAdoptedChild1);
        System.out.println(testAdoptedChild1.describeAge());
    }
}