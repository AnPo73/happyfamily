package main;

import java.io.Serializable;
import java.util.Set;

public class Fish extends Pet implements Serializable {

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        species = Species.FISH;
    }

    public Fish() {
    }

    @Override
    public void respond() {
        System.out.println("Привет, хозяин. Я - " + Species.FISH + " " + this.getNickname() + ". Я умею плавать.");
    }
}