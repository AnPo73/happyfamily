package main;

import java.io.Serializable;
import java.util.*;

public class Family implements Serializable {

    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pet;

    public Family() {
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children = new ArrayList<Human>(0);
        pet = new HashSet<>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    @Override
    public String toString() {
        return "Family" + "\n" +
                "mother=" + mother + "\n" +
                "father=" + father + "\n" +
                "children=" + children + "\n" +
                "pet=" + pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    protected void finalize() {
        System.out.println("Удаляется объект класса Family");
    }

    public String prettyFormat() {
        StringBuilder strChildren = new StringBuilder();
        for (Human child : children) {
            String strGender = (Objects.equals(child.getGender(), Gender.FEMALE) ? "girl" : "boy");
            strChildren.append("\n").append("\t").append("\t").append(strGender).append(": ").append(child.prettyFormat());
        }
        String string = "family:" + "\n" +
                "\t" + "mother: " + this.mother.prettyFormat() + "\n" +
                "\t" + "father: " + this.father.prettyFormat() + "\n" +
                "\t" + "children:" +
                (strChildren.toString().equals("") ? (" []") : (strChildren.toString())) + "\n" +
                "\t" + "pets: " + pet;
        System.out.println(string);
        return string;
    }

    public void addChild(Human child) {
        for (Human human : children) {
            if (human.equals(child)) {
                return;
            }
        }
        children.add(child);
        child.setFamily(this);
    }

    public void deleteChild(Human child) {
        if (checkIfChildExists(child)) {
            children.remove(child);
        }
    }

    private boolean checkIfChildExists(Human child) {
        for (Human human : children) {
            if (human.equals(child)) {
                return true;
            }
        }
        return false;
    }

    public void deleteChild(int index) {
        if (index < children.size()) {
            children.remove(index);
        }
    }

    public String countFamily() {
        if (children == null) {
            return "В этой семье " + 2 + " человека";
        }
        return "В этой семье " + (children.size() + 2) + " человека";
    }

    public int countFamilyInt() {
        if (children == null) {
            return 2;
        }
        return children.size() + 2;
    }
}