package main;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {

    private CollectionFamilyDao familyDao;
    private Random random = new Random();

    public FamilyService(CollectionFamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public List<Family> displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        families.forEach(Family::prettyFormat);
        return families;
    }

    public List<Family> getFamiliesBiggerThen(int size) {
        List<Family> updateFamilies = familyDao.getAllFamilies().stream().filter(family -> family.countFamilyInt() > size).toList();
        updateFamilies.forEach(Family::prettyFormat);
        return updateFamilies;
    }

    public List<Family> getFamiliesLessThen(int size) {
        List<Family> updateFamilies = familyDao.getAllFamilies().stream().filter(family -> family.countFamilyInt() < size).toList();
        updateFamilies.forEach(Family::prettyFormat);
        return updateFamilies;
    }

    public int countFamiliesWithMemberNumber(int size) {
        List<Family> updateFamilies = familyDao.getAllFamilies().stream().filter(family -> family.countFamilyInt() == size).toList();
        return updateFamilies.size();
    }

    public void createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String girlName, String boyName) throws ParseException {
        boolean isGirl = random.nextBoolean();
        String childName = isGirl ? girlName : boyName;
        Gender childGender = isGirl ? Gender.FEMALE : Gender.MALE;
        LocalDate dateNow = LocalDate.now();
        String currentDate = dateNow.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        family.addChild(new Human(childName, family.getFather().getSurname(), currentDate, childGender));
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        List<Family> updateFamilies = familyDao.getAllFamilies().stream().map(family -> deleteAllChildByAge(family, age)).collect(Collectors.toList());
        familyDao.setFamilyList(updateFamilies);
    }

    public int count() {
        List<Family> families = familyDao.getAllFamilies();
        return families.size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public List<Family> loadData(List<Family> families) {
        return familyDao.loadData(families);
    }

    public List<Family> loadTestData(List<Family> families) {
        return familyDao.loadTestData(families);
    }

    public Set<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family.getMother() != null) {
            return family.getPet();
        }
        return Collections.emptySet();
    }

    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family.getMother() != null) {
            Set<Pet> pets = family.getPet();
            pets.add(pet);
        }
    }

    public void saveData(List<Family> families) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("familyList.txt"))) {
            objectOutputStream.writeObject(families);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Family deleteAllChildByAge(Family family, int age) {
        int currentYear = LocalDate.now().getYear();
        List<Human> children = family.getChildren().stream().filter(child -> (currentYear - child.getYear() <= age)).collect(Collectors.toList());
        family.setChildren(children);
        return family;
    }
}