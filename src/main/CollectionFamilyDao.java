package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familyList;

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index > familyList.size()) {
            return new Family();
        } else {
            return familyList.get(index);
        }
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < familyList.size()) {
            familyList.remove(index);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (familyList.stream().anyMatch(f -> Objects.equals(f, family))) {
            familyList.remove(family);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void saveFamily(Family family) {
        Optional<Family> familyTempOptional = familyList.stream().filter(f -> Objects.equals(f, family)).findFirst();
        if (familyTempOptional.isPresent()) {
            Family familyTemp = familyTempOptional.get();
            int index = familyList.indexOf(familyTemp);
            deleteFamily(familyTemp);
            familyList.add(index, family);
        } else {
            familyList.add(family);
        }
    }

    @Override
    public List<Family> loadData(List<Family> families) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("familyList.txt"))) {
            families = (List<Family>) objectInputStream.readObject();
            families.forEach(Family::prettyFormat);
            if (familyList.size() > 0) {
                familyList.clear();
            }
            familyList.addAll(families);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return families;
    }

    @Override
    public List<Family> loadTestData(List<Family> families) {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("testDataList.txt"))) {
            families = (List<Family>) objectInputStream.readObject();
            families.forEach(Family::prettyFormat);
            if (familyList.size() > 0) {
                familyList.clear();
            }
            familyList.addAll(families);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return families;
    }

    public void setFamilyList(List<Family> familyList) {
        this.familyList = familyList;
    }
}