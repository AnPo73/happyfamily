package main;

public final class Woman extends Human {

    @Override
    public void greetPet(String petName) {
        System.out.println("Привет, " + petName + ". Рада тебя видеть.");
    }

    public void makeup() {
        System.out.println("Надо сделать макияж");
    }
}